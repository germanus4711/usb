#!/bin/bash
#
# Helper utility to construct the needed usb configurations for unraid 6.8.3
#
# It will compose the udev rules and the libvirt xml config files in a named subdirectory
#
# section for development
#
# example of udev_info
# P: /devices/pci0000:40/0000:40:01.1/0000:41:00.0/0000:42:08.0/0000:47:00.3/usb11/11-4/11-4.2/11-4.2.4 N: bus/usb/011/025 E: BUSNUM=011 E: DEVNAME=/dev/bus/usb/011/025 E: DEVNUM=025 E: DEVPATH=/devices/pci0000:40/0000:40:01.1/0000:41:00.0/0000:42:08.0/0000:47:00.3/usb11/11-4/11-4.2/11-4.2.4 E: DEVTYPE=usb_device E: DRIVER=usb E: ID_BUS=usb E: ID_MODEL=Saitek_Fip E: ID_MODEL_ENC=Saitek\x20Fip E: ID_MODEL_FROM_DATABASE=Pro Flight Instrument Panel E: ID_MODEL_ID=a2ae E: ID_REVISION=0300 E: ID_SERIAL=Saitek_Saitek_Fip_MZD2908FEB E: ID_SERIAL_SHORT=MZD2908FEB E: ID_USB_INTERFACES=:030000:ff0000: E: ID_VENDOR=Saitek E: ID_VENDOR_ENC=Saitek E: ID_VENDOR_FROM_DATABASE=Saitek PLC E: ID_VENDOR_ID=06a3 E: MAJOR=189 E: MINOR=1304 E: PRODUCT=6a3/a2ae/300 E: SUBSYSTEM=usb E: TYPE=0/0/0 E: USEC_INITIALIZED=1615871398
#
#
# get the options
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
subdir="unraid_usb"
verbose=0
address=0
managed_str=""

function show_help() {
	me="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
	echo -e "Usage: $me [-h?amvd ./unraid_usb] USB_BUS_ID VM-NAME
\th or ? shows this help text
\ta adds an address line
\tm sets the managed attribute
\tv switched on verbose mode
\td directory-path will create a subdirectory for the output
\tUSB_BUS_ID is the bus number including leading 0's
\tVM-NAME is the VM name the usb devices shall be attached to (Spaces MUST be escaped!)
\tThe output files will ibe created under directory-path/xml and directory-path/config"
}

while getopts "h?amvd:" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    v)  verbose=1
        ;;
    a)  address=1
        ;;
    m)  managed_str=" managed='yes'"
        ;;
    d)  subdir=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

# get arg parameter and name them
usb_bus=$1
vmname="$2"

cmd="lsusb"
cmd_options=""

# get the # of device w/o the hubs
num_devices=$(lsusb -s ${1}: | grep -vi "hub" | wc -l)

cmd_options="-s ${usb_bus}:"

rm -rf ${subdir}
mkdir -p ${subdir}/xml ${subdir}/config
touch ${subdir}/config/90-libvirt-usb.rules

function get_vendor_id() {
	_info=$1
	echo "$_info" | sed -n -e '/ID_VENDOR_ID/ s/.*\=//p'
}

function get_model_id() {
	_info=$1
	echo "$_info" | sed -n -e '/ID_MODEL_ID/ s/.*\=//p'
}

function get_product(){
	_info=$1
	echo "$_info" | sed -n -e '/PRODUCT/ s/.*\=//p'
}


i=1
while [ $i -lt $num_devices ];
do
	sed_str="${i}{p;q}"
	device_info=$($cmd "$cmd_options" | sed -n -e "$sed_str")
	usb_bus=$(echo "$device_info" | /usr/bin/awk -F ' ' '{print $2}')
	device_num=$(echo "$device_info" | /usr/bin/awk -F ' ' '{print $4}' | tr -d ":")
	udev_info=$(udevadm info /dev/bus/usb/$usb_bus/$device_num)
	vendor_id=$(get_vendor_id "$udev_info")
	model_id=$(get_model_id "$udev_info")
	product=$(get_product "$udev_info")

	# format string to output the config xml
	config="<hostdev mode='subsystem' type='usb'${managed_str}>
\t<source startupPolicy='optional'>
\t\t<vendor id='0x${vendor_id}'/>
\t\t<product id='0x${model_id}'/>
\t</source>"
	if [ "$address" -eq "1" ]; then
		config+="\n\t<address type='usb' bus='$(expr ${usb_bus} + 0)' port='$(expr ${device_num} + 0)'/>"
	fi
	config+="\n</hostdev>"

	# format string for the rules file
	rules="ACTION==\"add\", \\
\tENV{PRODUCT}==\"${product}\", \\
\tRUN+=\"/boot/config/callusbhotplug.sh 'add' '${vmname}' '${vendor_id}' '${model_id}'\"
ACTION==\"remove\", \\
\tENV{PRODUCT}==\"${product}\", \\
\tRUN+=\"/boot/config/callusbhotplug.sh 'remove' '${vmname}' '${vendor_id}' '${model_id}'\""


	if [ ! -z "$vendor_id" ]; then
		if ! test -f "${subdir}/xml/usb-${vendor_id}-${model_id}.xml"; then
			echo -e "$rules" >> ${subdir}/config/90-libvirt-usb.rules
		fi
		echo -e "$config" >> ${subdir}/xml/usb-${vendor_id}-${model_id}.xml
		if [ "$verbose" -eq "1" ]; then
			echo "Done: [${usb_bus}.${device_num}] ${vendor_id}:${model_id}"
		fi
	fi
	let i+=1
done
